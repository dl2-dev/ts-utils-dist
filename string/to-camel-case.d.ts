/**
 * Returns the given `str` in `camelCased` format.
 */
export default function toCamelCase(str: string, upperFirst?: boolean): string;

/**
 * Replace all punctuation characters from the given `str` with
 * the given `replacement`.
 */
export default function removePunctuation(str: string, replacement?: string): string;

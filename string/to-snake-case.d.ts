/**
 * Returns the given `str` in `snake_cased` format.
 *
 * @note separator **MUST** have 1 (one) char only
 */
export default function toSnakeCase(str: string, separator?: string): string;

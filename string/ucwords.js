"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const REGEXP_MATCHER = /^(.)|\s+(.)/g;
/**
 * Uppercase the first character of each word in the given `str`.
 */
function ucwords(str) {
    return str.toLocaleLowerCase().replace(REGEXP_MATCHER, ($1) => {
        return $1.toLocaleUpperCase();
    });
}
exports.default = ucwords;
//# sourceMappingURL=ucwords.js.map
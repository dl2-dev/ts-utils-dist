/**
 * Uppercase the first character of each word in the given `str`.
 */
export default function ucwords(str: string): string;

"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const to_camel_case_1 = __importDefault(require("./to-camel-case"));
const REGEXP_MATCHER = /([A-Z])/g;
/**
 * Returns the given `str` in `snake_cased` format.
 *
 * @note separator **MUST** have 1 (one) char only
 */
function toSnakeCase(str, separator = "_") {
    return to_camel_case_1.default(str, false)
        .replace(REGEXP_MATCHER, separator + "$1")
        .toLowerCase();
}
exports.default = toSnakeCase;
//# sourceMappingURL=to-snake-case.js.map
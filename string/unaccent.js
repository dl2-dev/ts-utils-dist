"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const REGEXP_MATCHER = /[\u0300-\u036f]/g;
/**
 * Removes accents (diacritic signs) from the given `str`.
 *
 * @param string the input string
 *
 * @return the cleaned string
 */
function unaccent(str) {
    return str.normalize("NFD").replace(REGEXP_MATCHER, "");
}
exports.default = unaccent;
//# sourceMappingURL=unaccent.js.map
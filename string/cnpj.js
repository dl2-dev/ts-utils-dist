"use strict";
/* eslint-disable @typescript-eslint/ban-ts-comment */
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Validate and, optionally, format brazilian CNPJ strings.
 */
function cnpj(input, format = false) {
    const ERRORMSG = `type '${input}' is not assignable to type 'cnpj'`;
    input = input.replace(/\D+/g, "");
    if (input.length !== 14 || new RegExp(`${input[0]}{14}`).test(input)) {
        throw new Error(ERRORMSG);
    }
    const digits = [6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2];
    let [i, n] = [0, 0];
    // @ts-ignore 2367
    for (; i < 12; n += input[i] * digits[++i]) {
        //
    }
    // @ts-ignore 2367
    if (input[12] != ((n %= 11) < 2 ? 0 : 11 - n)) {
        throw new Error(ERRORMSG);
    }
    // @ts-ignore 2367
    for (i = 0, n = 0; i <= 12; n += input[i] * digits[i++]) {
        //
    }
    // @ts-ignore 2367
    if (input[13] != ((n %= 11) < 2 ? 0 : 11 - n)) {
        throw new Error(ERRORMSG);
    }
    if (!format) {
        return input;
    }
    return (input.substr(0, 2) +
        "." +
        input.substr(2, 3) +
        "." +
        input.substr(5, 3) +
        "/" +
        input.substr(8, 4) +
        "-" +
        input.substr(12));
}
exports.default = cnpj;
//# sourceMappingURL=cnpj.js.map
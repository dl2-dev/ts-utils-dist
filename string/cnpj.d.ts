/**
 * Validate and, optionally, format brazilian CNPJ strings.
 */
export default function cnpj(input: string, format?: boolean): string;

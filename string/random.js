"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const crypto_1 = require("crypto");
const remove_punctuation_1 = __importDefault(require("./remove-punctuation"));
const ALLOWED_CHARS = "ABCDEFGHJKLMNPRTUVWXYZabcdefghjkmnopqrtuvwxyz346789";
/**
 * Generate "safe" pseudorandom strings.
 *
 * @note allowedChars **MUST** contain alphanumeric chars only
 */
function random(length = 24, allowedChars = ALLOWED_CHARS) {
    let chars = remove_punctuation_1.default(allowedChars);
    if (!chars) {
        chars = ALLOWED_CHARS;
    }
    const filterFn = new RegExp(`[^${chars}]`, "g");
    let result = "";
    while (result.length < length) {
        result += crypto_1.randomBytes(length).toString("hex").replace(filterFn, "");
    }
    return result.substr(0, length);
}
exports.default = random;
//# sourceMappingURL=random.js.map
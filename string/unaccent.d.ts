/**
 * Removes accents (diacritic signs) from the given `str`.
 *
 * @param string the input string
 *
 * @return the cleaned string
 */
export default function unaccent(str: string): string;

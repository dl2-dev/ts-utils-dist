"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Returns a string with the first character of the given
 * `str` capitalized.
 */
function ucfirst(str) {
    return str.charAt(0).toLocaleUpperCase() + str.substr(1).toLocaleLowerCase();
}
exports.default = ucfirst;
//# sourceMappingURL=ucfirst.js.map
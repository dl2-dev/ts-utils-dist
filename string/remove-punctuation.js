"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const REGEXP_MATCHER = /[\u2000-\u206F\u2E00-\u2E7F\\'!"#$%&()*+,\-./:;<=>?@[\]^_`{|}~]/g;
/**
 * Replace all punctuation characters from the given `str` with
 * the given `replacement`.
 */
function removePunctuation(str, replacement = "") {
    return str.replace(REGEXP_MATCHER, replacement).replace(/\s+/g, " ").trim();
}
exports.default = removePunctuation;
//# sourceMappingURL=remove-punctuation.js.map
"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ucfirst_1 = __importDefault(require("./ucfirst"));
const unaccent_1 = __importDefault(require("./unaccent"));
const REGEXP_CAMEL = /([A-Z]{1,})/g;
const REGEXP_MATCHER = /[A-Z\xC0-\xD6\xD8-\xDE]?[a-z\xDF-\xF6\xF8-\xFF\d]+/g;
/**
 * Returns the given `str` in `camelCased` format.
 */
function toCamelCase(str, upperFirst = false) {
    if (str.length === 1) {
        if (upperFirst) {
            return str.charAt(0).toLocaleUpperCase();
        }
        return str;
    }
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    str = unaccent_1.default(str)
        .replace(REGEXP_CAMEL, "-$1")
        .toLowerCase()
        .match(REGEXP_MATCHER)
        .reduce((result, word) => {
        return result + ucfirst_1.default(word);
    });
    if (upperFirst) {
        return str.charAt(0).toLocaleUpperCase() + str.substr(1);
    }
    return str;
}
exports.default = toCamelCase;
//# sourceMappingURL=to-camel-case.js.map
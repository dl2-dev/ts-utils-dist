/**
 * Generate "safe" pseudorandom strings.
 *
 * @note allowedChars **MUST** contain alphanumeric chars only
 */
export default function random(length?: number, allowedChars?: string): string;

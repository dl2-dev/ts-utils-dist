/**
 * Validate and, optionally, format brazilian CPF strings.
 */
export default function cpf(input: string, format?: boolean): string;

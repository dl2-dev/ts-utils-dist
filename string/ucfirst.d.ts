/**
 * Returns a string with the first character of the given
 * `str` capitalized.
 */
export default function ucfirst(str: string): string;

"use strict";
/* eslint-disable @typescript-eslint/ban-ts-comment */
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Validate and, optionally, format brazilian CPF strings.
 */
function cpf(input, format = false) {
    const ERRORMSG = `type '${input}' is not assignable to type 'cpf'`;
    input = input.replace(/\D+/g, "");
    if (input.length !== 11 || new RegExp(`${input[0]}{11}`).test(input)) {
        throw new Error(ERRORMSG);
    }
    let [i, n, s] = [0, 0, 10];
    // @ts-ignore 2367
    for (; s >= 2; n += input[i++] * s--) {
        //
    }
    // @ts-ignore 2367
    if (input[9] != ((n %= 11) < 2 ? 0 : 11 - n)) {
        throw new Error(ERRORMSG);
    }
    // @ts-ignore 2367
    for (s = 11, n = 0, i = 0; s >= 2; n += input[i++] * s--) {
        //
    }
    // @ts-ignore 2367
    if (input[10] != ((n %= 11) < 2 ? 0 : 11 - n)) {
        throw new Error(ERRORMSG);
    }
    if (!format) {
        return input;
    }
    return (input.substr(0, 3) +
        "." +
        input.substr(3, 3) +
        "." +
        input.substr(6, 3) +
        "-" +
        input.substr(9));
}
exports.default = cpf;
//# sourceMappingURL=cpf.js.map
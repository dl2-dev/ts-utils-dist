"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Super fast memoize for functions.
 */
function memoize(fn) {
    const cache = new Map();
    return (input) => {
        let output = cache.get(input);
        if (output === undefined) {
            output = fn.apply(null, [input]);
            cache.set(input, output);
        }
        return output;
    };
}
exports.default = memoize;
//# sourceMappingURL=memoize.js.map
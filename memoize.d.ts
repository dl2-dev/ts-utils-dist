declare type MemoizeFn = (str: string) => string;
/**
 * Super fast memoize for functions.
 */
export default function memoize(fn: MemoizeFn): MemoizeFn;
export {};
